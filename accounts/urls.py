from django.urls import path
from . import views
from accounts.views import logout_view, user_signup


urlpatterns = [
    path('login/', views.login_user, name='login'),
    path('logout/', logout_view, name='logout'),
    path('signup/', views.user_signup, name='signup'),
]
