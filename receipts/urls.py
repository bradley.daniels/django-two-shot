from django.urls import path
from receipts.views import receipts_list

print("receipts app urls loaded")

urlpatterns = [
    path("", receipts_list, name="home")
]
