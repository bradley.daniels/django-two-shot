from django.shortcuts import render
from .models import Receipt
from django.contrib.auth.decorators import login_required

@login_required
def receipts_list(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    return render(request, "receipts/list.html", {"receipts": receipts})
